﻿using Object;
using System.Runtime.CompilerServices;

namespace Ass1
{
    internal class Program
    {
       
        static void Main(string[] args)
        {
            Teacher teacher = new Teacher();
            List<Teacher> list = new List<Teacher>(){
            new Teacher() { fullName = "Alice", birthDate = DateOnly.Parse("10/20/2002") ,salary = 10000, SalaryFactor = 2 },
            new Teacher() { fullName = "Doe", birthDate = DateOnly.Parse("2/1/2002") ,salary = 7000, SalaryFactor = 3 },
            new Teacher() { fullName = "John", birthDate = DateOnly.Parse("1/20/2002") ,salary = 6000, SalaryFactor = 4 }
        };
            Console.WriteLine("Employee Management System");
   

            while (true)
            {
                Console.WriteLine("Menu:");
                Console.WriteLine("1. Enter new employee");
                Console.WriteLine("2. Display lowest salary employees");
                Console.WriteLine("3. Exit");

                Console.WriteLine("Enter your choice: ");
                int choice = int.Parse(Console.ReadLine());

                switch (choice)
                {
                    case 1:
                        if (teacher.EnterNew() == true)
                        {
                            list.Add(teacher);
                            Console.WriteLine("Add new employee succesfully");
                        }                    
                        break;
                    case 2:
                        Teacher temp = list.MinBy(teach => teach.SalaryCal());
                        temp.Display();
                        break;
                    case 3:
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("Invalid choice.");
                        break;
                }
            }
        }
    }
}