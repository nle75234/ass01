﻿namespace Object

{

    public class Employee
    {
        public String fullName { get; set; }
        public DateOnly birthDate { get; set; }
        public float salary { get; set; }

        public Employee(string fullName, DateOnly birthDate, float salary)
        {
            this.fullName = fullName;
            this.birthDate = birthDate;
            this.salary = salary;
        }
        public Employee() { }

        public virtual bool EnterNew()
        {
            Console.Write("Enter Fullname : ");
            fullName = Console.ReadLine();
            if (fullName == null || fullName.Trim() == "")
            {
                Console.WriteLine("Fullname can not be null");
                return false;

            }

            Console.Write("Enter birthDate : ");
            string birthDateString = Console.ReadLine();
            try
            {
                // Try to parse the string as a DateOnly value.
                birthDate = DateOnly.Parse(birthDateString);
            }
            catch (FormatException)
            {
                // The string is not in a valid format.
                Console.WriteLine("The birth date is not in a valid format.");
                return false;
            }


            Console.Write("Enter basic salary : ");
            salary = float.Parse(Console.ReadLine());
            if (salary < 0)
            {
                Console.WriteLine("The basic salary cannot be negative.");
                return false;
            }

            return true;
        }
        public virtual void Display()
        {
                Console.WriteLine("Hoten :" +fullName +" ; Birth Date : " +birthDate +" ; Basic Salary : "+salary);
        }


        public virtual float SalaryCal()
        {
            return salary;
        }

    }
}