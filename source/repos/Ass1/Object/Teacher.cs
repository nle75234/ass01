﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Object
{
    public class Teacher : Employee

    {
        public Teacher()
        {
        }
        public float SalaryFactor { get; set; }

        public Teacher(string fullName, DateOnly birthDate, float salary, float SalaryFactor) : base(fullName, birthDate, salary)
        {
            this.SalaryFactor = SalaryFactor;
        }

        

        private List<Teacher> teacherList = new List<Teacher>();
        public override void Display()
        {
            Console.WriteLine("Hoten :" + fullName + " ; Birth Date : " + birthDate + " ; Basic Salary : " + salary + " ; Total Salary : " + salary*SalaryFactor*1.25);
        }

        public override bool EnterNew()
        {
            Console.Write("Enter Fullname : ");
            fullName = Console.ReadLine();
            if (fullName == null || fullName.Trim() == "")
            {
                Console.WriteLine("Fullname can not be null");
                return false;
                
            }

            Console.Write("Enter birthDate : ");
            string birthDateString = Console.ReadLine();
            try
            {
                // Try to parse the string as a DateOnly value.
                birthDate = DateOnly.Parse(birthDateString);
            }
            catch (FormatException)
            {
                // The string is not in a valid format.
                Console.WriteLine("The birth date is not in a valid format.");
                return false;
            }


            Console.Write("Enter basic salary : ");
            salary = float.Parse(Console.ReadLine());
            if (salary < 0)
            {
                Console.WriteLine("The basic salary cannot be negative.");
                return false;
            }

            Console.Write("Enter salary factor : ");
            SalaryFactor = float.Parse(Console.ReadLine());
            if (SalaryFactor < 0)
            {
                Console.WriteLine("The salary factor cannot be negative.");
                return false;
            }
            return true;
        }
      
        public override float SalaryCal()
        {
            return (float)(base.SalaryCal() * SalaryFactor * 1.25);
        }
    }
}
